
import botogram
import functions
import config

bot = botogram.create(config.token)

@bot.command("start")
def start(chat, message, args):
    chat.send("*Willkommen im FFF-Newsletter!*\nHier gibt es alle aktuellen Infos zu Fridays for Future.\nUnter Kategorien kannst du auswählen, welche Nachrichten du erhalten möchtest. Wenn du keine Nachrichten mehr empfangen möchtest, drücke auf Stopp.")

@bot.command("kategorien")
def kategorien(chat, message, args):
    cats = botogram.Buttons()
    cats[0].callback("Weekly ✅", "weekly")
    cats[1].callback("News ✅", "news")
    cats[2].callback("Presse ✅", "press")
    cats[3].callback("Bot ✅", "botnews")
    chat.send("*Kategorien*\nDu kannst hier auswählen, aus welchen Kategorien du Nachrichten erhalten möchtest.\n\n📆 Weekly\n📂 Wöchentliche Zusammenfassung\n✉️ eine Nachricht pro Woche\n\n🗞 News\n📂 Aktuelle Neuigkeiten\n✉️ seltener, max. eine Nachricht pro Tag\n\n🎙 Presse\n📂 Presseinformationen\n✉️ seltener, je nach Tag und Aktivität\n\n🤖 Bot\n📂 Neuigkeiten zum Bot und zu neuen Funktionen\n✉️ selten", attach=cats)



if __name__ == "__main__":
    bot.run()

